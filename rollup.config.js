import {terser} from 'rollup-plugin-terser';
import resolve from 'rollup-plugin-node-resolve';
import globals from 'rollup-plugin-node-globals';

import commonjs from 'rollup-plugin-commonjs';

import json from 'rollup-plugin-json';
import replace from 'rollup-plugin-replace';

// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;
let apiHost = process.env.API_HOST || 'http://localhost:8000/hub/v1';
let dist = production && 'dist/unk-' + (process.env.VERSION || 'beta') + '.js';
console.log(apiHost);
console.log(dist);
/**
 *
 */
export default {
  input: 'src/main.js',
  format: 'iife',
  output: {
    file: production ? dist : 'test/bundle.js',
    format: 'iife', // immediately-invoked function expression — suitable for <script> tags
    sourcemap: !production,
  },
  plugins: [
    replace({
      values: {
        'API_HOST': apiHost,
      },
    }),
    resolve({jsnext: true, preferBuiltins: true, browser: true, main: true}),
    commonjs({
      // non-CommonJS modules will be ignored, but you can also
      // specifically include/exclude files
      include: 'node_modules/**',  // Default: undefined
      browser: true,
      preferBuiltins: false,
      // if true then uses of `global` won't be dealt with by this plugin
      ignoreGlobal: false,  // Default: false

      // if false then skip sourceMap generation for CommonJS modules
      sourceMap: false  // Default: true

      // explicitly specify unresolvable named exports
      // (see below for more details)
      // namedExports: { './module.js': ['foo', 'bar' ] }  // Default: undefined
    }),
    json(),
    globals,
    production && terser() // minify, but only in production
  ]
};
