The client sdk for [unknownanalytics](unknownanalytics.com/).

In red square : 

![module](docs/sdk.arch.png)
This sdk send a default page view event once the sdk is loaded within the current page.
It listens to url changes via `pushState` to handle SPA applications (page views changes). Common events supported now : clicks, mousemove and mouseover.

#roadmaps : 

- screen recording
- scroll map  


_Only simple XHR for now are supported._ 





#### production 

`rollup -c --environment API_HOST:www.unknonwnanalytics.com/collect,VERSION:beta-0`


#### staging

`rollup -c --environment API_HOST:staging.unknonwnanalytics.com/collect,VERSION:beta-0`


#### dev

`rollup -c --environment API_HOST:localhost:82/collect,VERSION:beta-0`
