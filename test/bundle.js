(function () {
  'use strict';

  /**
   *
   */
  class Sender {
    /*
    wsInstance: WebSocket = null;
    config: {} = null;*/

    constructor(config) {
      this.config = config;
      // WS is not supported
      /*if (!this.wsInstance) {
          this.wsInstance = this._initWS();
      }*/
    }

    /**
     *
     * @returns {null|WebSocket}
     */
    _initWS() {
      if ("WebSocket" in window) {
        // Let us open a web socket
        let ws = new WebSocket(config.pushURL);

        ws.onopen = () => {
          console.log("cnx");
        };

        ws.onmessage = (evt) => {
          console.log("Message received");
        };
        ws.onclose = () => {
          console.log("cnx closed");
        };
        ws.onerror = () => {
          console.log("onerror");
        };
        return ws;
      } else {
        // The browser doesn't support WebSocket
        console.warn("WebSocket NOT supported by your Browser!");
        return null;
      }
    }

    /**
     * @param args {XHRArgs}
     * @private
     */
    _sendXHR(args) {
      var xhr = new XMLHttpRequest();
      var config = this.config;
      // : 'bearer ' + config.token
      xhr.open(args.method, args.url, true);
      let headers = args.headers || {};
      for (let key in headers) {
        xhr.setRequestHeader(key, headers[key]);
      }

      // Avoid the preflight request
      //xhr.setRequestHeader('Authorization', config.token);
      if (!headers['Content-type']) {
        xhr.setRequestHeader("Content-Type", "text/plain");
      }
      let data = args.data || null;
      if (!data.token) {
        data.token = config.token;
      }
      if (typeof (data) !== "string") {
        data = JSON.stringify(data);
      }
      // catch the callback
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          try {
            var response = JSON.parse(xhr.responseText);
            if (xhr.status === 200 && response.status === 'OK') {
              if (args.onSuccess) {
                args.onSuccess(response);
              }
              console.log('successful');
            } else {
              if (args.onError) {
                args.onError(response);
              }
              console.error('failed to execute ');
            }
          } catch (e) {
            console.log(xhr.status);
          }
        }
      };
      xhr.send(data || null);
    }

    /**
     *
     * @param args
     */
    _sendRequest(args) {
      args = args || {};
      if (!args.method) {
        options.method = 'GET';
      }
      // check if is async mode
      if (args.sync) {
        this._sendXHR(args);
      } else {
        // do not block the app
        setTimeout(() => {
          this._sendXHR(args);
        });
      }
    }

    /**
     *
     * @param args
     * @returns {boolean}
     */
    push(args) {
      let endpoint = args.endpoint;
      // WS not supported yet
      if (this.wsInstance) {
        let data = args.data;
        data.endpoint = endpoint;
        data = JSON.stringify(args.data);
        this.wsInstance.send(data);
        return true;
      }
      args = args || {};
      args.method = 'POST';
      args.url = this.config.apiURL + '/' + endpoint;
      this._sendRequest(args);
    }
  }

  /**
   *
   * @type {{getBrowserParams(): *, sendXHR(*=): void, _sendXHR(*): void, parse(String): (*|*|*), postXHR(*=): void, utm(*=): *}}
   */
  const Utils = {
    /**
     * Parse the given query `str`.
     *
     * @param {String} str
     * @return {Object}
     * @api public
     */
    parse(str) {
      function decode(str) {
        try {
          return decodeURIComponent(str.replace(/\+/g, ' '));
        } catch (e) {
          return str;
        }
      }

      var pattern = /(\w+)\[(\d+)\]/;
      if ('string' != typeof str) return {};
      str = str.trim();
      if ('' === str) return {};
      if ('?' === str.charAt(0)) str = str.slice(1);

      var obj = {};
      var pairs = str.split('&');
      for (var i = 0; i < pairs.length; i++) {
        var parts = pairs[i].split('=');
        var key = decode(parts[0]);
        var m;
        if (m = pattern.exec(key)) {
          obj[m[1]] = obj[m[1]] || [];
          obj[m[1]][m[2]] = decode(parts[1]);
          continue;
        }

        obj[parts[0]] = null == parts[1]
            ? ''
            : decode(parts[1]);
      }
      return obj;
    },
    /**
     * Utm parsing
     * @param query
     */
    utm(query) {
      // Remove leading ? if present
      if (query.charAt(0) === '?') {
        query = query.substring(1);
      }
      query = query.replace(/\?/g, '&');
      let param;
      let params = Utils.parse(query);
      let results = {};
      for (var key in params) {
        if (Object.hasOwnProperty.call(params, key)) {
          if (key.substr(0, 4) === 'utm_') {
            param = key.substr(4);
            if (param === 'campaign') param = 'name';
            results[param] = params[key];
          }
        }
      }
      return results;
    },
    /**
     * @returns {BrowserParams}
     *  The browser information
     */
    getBrowserParams() {
      return {
        // user agent
        ua: navigator.userAgent,
        // window width
        ww: window.innerWidth,
        // window height
        wh: window.innerHeight
      }
    },
    /**
     * Get array approximately the size of array
     */
    getArraySize(array) {
      return (JSON.stringify(array).replace(/[\[\]\,\"]/g, '')).length;
    },
  };

  const END_POINT$1 = 'view';

  /**
   *
   */
  class PageViewManager {
      /*
      sender: Sender = null;
      options: null;
      */



      /**
       *
       * @param options
       * @param sender
       */
      constructor(options, sender) {
          this.options = options;
          this.sender = sender;
          let eventType = 'pushState';
          (function (history) {
              var pushState = history.pushState;
              history.pushState = function (state) {
                  if (typeof history.onpushstate == "function") {
                      history.onpushstate({state: state});
                  }
                  var event = new Event(eventType);
                  event.arguments = arguments;
                  window.dispatchEvent(event);
                  // ... whatever else you want to do
                  // maybe call onhashchange e.handler
                  return pushState.apply(history, arguments);
              };
          })(window.history);

          // Now we can listen for pushState events and keep the original feature of the browser working
          window.addEventListener(eventType, this.handleUrl.bind(this));

          // init first call
          this.fireFirstEvent();
      }

      /**
       *
       */
      fireFirstEvent() {
          this.handleUrl();
      }

      /**
       *
       */
      handleUrl(event) {
          let location = document.location;
          let href = location.href;
          if (!(location.hostname === 'localhost' || location.origin.startsWith('https'))) {
              return console.error('only https is allowed');
          }
          let data = Object.assign({}, {uri: href}, {utm: Utils.utm(href)});
          this.push(data);
      }

      /**
       *
       */
      push(body) {
          body = Object.assign({}, body, {browser: Utils.getBrowserParams()});
          this.sender.push({data: body, endpoint: END_POINT$1});
      }
  }

  const MOUSE_MOVE_EVENT = 'mousemove';
  const MOUSE_OVER_EVENT = 'mouseover';
  const CLICK_EVENT = 'click';
  const END_POINT$2 = 'event';

  /**
   *
   */
  class EventManager {
      /*
          sender: Sender = null;
          options: null;
          */


      /**
       *
       */
      _initializeQuery() {
          this.query = [];
          this.querySize = 0;
      }

      /**
       *
       */
      resetQuery() {
          this._initializeQuery();
      }

      /**
       *
       * @param options
       * @param sender
       */
      constructor(options, sender) {
          this.options = options;
          this.sender = sender;
          this._initializeQuery();
          this._configureEvents();
      }

      /**
       *
       * @private
       */
      _configureEvents() {
          this._configureEvent(MOUSE_MOVE_EVENT,);
          this._configureEvent(CLICK_EVENT);
          this._configureEvent(MOUSE_OVER_EVENT);
      }

      /**
       *
       * @private
       */
      _configureEvent(eventType, options) {
          /**
           *
           * @type {boolean}
           */
          this.activateMouseMove = false;
          // Listen to mousemove
          window.addEventListener(eventType, function (event) {
              if (eventType !== MOUSE_MOVE_EVENT) {
                  this.handleEvent(eventType, Utils.getBrowserParams(), event);
              }
              // M
              else {
                  if (this.activateMouseMove) {
                      this.handleEvent(eventType, Utils.getBrowserParams(), event);
                  }
              }
          }.bind(this));
          // do not block the app
          if (eventType === MOUSE_MOVE_EVENT)
              setInterval(function () {
                  this.activateMouseMove = !this.activateMouseMove;
              }.bind(this), 100);
      }

      /**
       *
       * @param eventType
       * @param params {BrowserParams}
       * @param event
       */
      handleEvent(eventType, params, event) {
          let body = document.body,
              html = document.documentElement;
          let height = Math.max(body.scrollHeight, body.offsetHeight,
              html.clientHeight, html.scrollHeight, html.offsetHeight);
          let px = (event.clientX + document.documentElement.scrollLeft) / screen.width * 100;
          let py = (event.clientY + document.documentElement.scrollTop) / height * 100;
          this.pushToQuery({
              px: px,
              py: py,
              at: Date.now(),
              eventType: eventType
          });
          console.log(eventType, px, py);

      }

      /**
       * @param data {EventPosition}
       */
      pushToQuery(data) {
          this.query.push(data);
          this.querySize += JSON.stringify(data).length * 8;
          if (this.querySize > 256000) {
              this.push(this.query);
              this.resetQuery();
          }
      }

      /**
       *
       */
      push(body) {
          body = Object.assign({}, body, {browser: Utils.getBrowserParams()});
          this.sender.push({data: body, endpoint: END_POINT$2});
      }

  }

  class ErrorManager {
      /*
      sender: Sender = null;
      options: null;
      */


      /**
       *
       */
      _initializeQuery() {
          this.query = [];
          this.querySize = 0;
      }

      /**
       *
       */
      resetQuery() {
          this._initializeQuery();
      }

      /**
       *
       * @param options
       * @param sender
       */
      constructor(options, sender) {
          this.options = options;
          this.sender = sender;
          this._initializeQuery();
          this._configureEvent();
      }

      /**
       *
       * @private
       */
      _configureEvent() {
          // Listen to mousemove
          window.onerror = (function (message, source, lineno, colno, error) {
              let body = document.body,
                  html = document.documentElement;
              let height = Math.max(body.scrollHeight, body.offsetHeight,
                  html.clientHeight, html.scrollHeight, html.offsetHeight);
              let px = (event.clientX + document.documentElement.scrollLeft) / screen.width * 100;
              let py = (event.clientY + document.documentElement.scrollTop) / height * 100;
              this.pushToQuery({
                  px: px,
                  py: py,
                  at: Date.now(),
                  error: arguments[1]
              });

              console.log(message);
          }).bind(this);
      }

      /**
       * @param data {EventPosition}
       */
      pushToQuery(data) {
          this.query.push(data);
          this.querySize += JSON.stringify(data).length * 8;
          if (this.querySize > 256000) {
              this.push(this.query);
              this.resetQuery();
          }
      }

      /**
       *
       */
      push(body) {
          body = Object.assign({}, body, {browser: Utils.getBrowserParams()});
          this.sender.push({data: body, endpoint: END_POINT});
      }
  }

  /**
   *
   */


  /**
   *
   * @type {{agent: string, ui: {w: number, h: number}}}
   */
  let config$1 = {
      apiURL: 'http://localhost:8000/hub/v1',
      wsURL: 'PUSH_URL',
      token: null
  };
  /**
   *
   * @param token
   * @param options
   */
  window.unknown = function (token, options) {
      config$1.token = token;
      let sender = new Sender(config$1);
      new PageViewManager(options, sender);
      new EventManager(options, sender);
      new ErrorManager(options, sender);
  };

}());
//# sourceMappingURL=bundle.js.map
